<?php

namespace Drupal\entity_trait;

use Drupal\Core\Plugin\PluginBase;

/**
 * Provides a base implementation for an entity_trait plugin.
 */
abstract class EntityTraitPluginBase implements EntityTraitPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function traitFieldDefinitions() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function traitEntityType() {
    return 'entity_trait_use';
  }

}