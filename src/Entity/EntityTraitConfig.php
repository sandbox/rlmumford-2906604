<?php

namespace Drupal\entity_trait\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the EntityTraitConfig entity.
 *
 * @ConfigEntityType(
 *   id = "entity_trait_config",
 *   label = @Translation("Trait Config"),
 *   config_prefix = "config",
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\entity_trait\Form\EntityTraitConfigAddForm",
 *       "edit" = "Drupal\entity_trait\Form\EntityTraitConfigForm"
 *     },
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "entity_trait",
 *     "target_entity_type_id",
 *     "target_bundle_id",
 *     "field_map",
 *   }
 * )
 */
class EntityTraitConfig extends ConfigEntityBundleBase implements EntityTraitConfigInterface {

  /**
   * The entity_trait_config ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The entity_trait_config label.
   *
   * @var string
   */
  protected $label;

  /**
   * The target entity type.
   *
   * @var string
   */
  protected $target_entity_type_id;

  /**
   * The target bundle.
   *
   * @var string
   */
  protected $target_bundle_id;

  /**
   * The entity trait plugin id.
   *
   * @var string
   */
  protected $entity_trait;

  /**
   * The field map for the entity trait fields.
   *
   * @var array
   */
  protected $field_map;

  /**
   * Other plugin settings.
   *
   * @var array
   */
  protected $settings;

  /**

  /**
   * {@inheritdoc}
   */
  public function getTargetEntityTypeId() {
    return $this->target_entity_type_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getTargetBundleId() {
    return $this->target_bundle_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getPlugin() {
    return \Drupal::service('plugin.manager.entity_trait_plugin')->createInstance(
      $this->entity_trait,
      [
        'field_map' => $this->field_map,
      ]
    );
  }

  /**
   * {@inheritdoc}
   *
   * Add entity traits fields.
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    ConfigEntityBase::postSave($storage, $update);

    $entity_manager = $this->entityManager();
    $entity_trait_use_type = $this->getPlugin()->traitEntityType();
    if (!$update) {
      // Create a new bundle of entity trait use.
      $entity_manager->onBundleCreate($this->id(), $entity_trait_use_type);

      // Create new fields on target.
      $old_definitions = $entity_manager->getFieldStorageDefinitions($this->target_entity_type_id);
      if (empty($old_definitions['entity_traits'])) {
        $entity_manager->clearCachedDefinitions();
        $definitions = $entity_manager->getFieldStorageDefinitions($this->target_entity_type_id);
        $entity_manager->onFieldStorageDefinitionCreate($definitions['entity_traits']);
      }
    }
    else {
      // Invalidate the render cache of entities for which this entity is a
      // bundle
      if ($entity_manager->hasHandler($entity_trait_use_type, 'view_builder')) {
        $entity_manager->getViewBuilder($entity_trait_use_type)->resetCache();
      }

      // Entity bundle field definitions may depend on bundle settings.
      $entity_manager->clearCachedFieldDefinitions();
      $entity_manager->clearCachedBundles();
    }
  }

  /**
   * {@inheritdoc}
   *
   * Clear up entity traits fields.
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    ConfigEntityBase::postDelete($storage, $entities);

    $types = [];
    foreach ($entities as $entity) {
      $entity->deleteDisplays();
      \Drupal::entityManager()->onBundleDelete($entity->id(), $entity->getPlugin()->traitEntityType());
      $types[$entity->get('target_entity_type_id')] = $entity->get('target_entity_type_id');
    }

    $entity_manager = \Drupal::entityManager();
    foreach ($types as $entity_type_id) {
      $types[$entity_type_id] = $entity_manager->getLastInstalledFieldStorageDefinitions($entity_type_id)['entity_traits'];
    }

    $entity_manager->clearCachedDefinitions();
    foreach ($types as $entity_type_id => $definitions) {
      $new_defs = $entity_manager->getFieldStorageDefinitions($entity_type_id);
      if (!empty($new_defs['entity_traits'])) {
        continue;
      }

      if (!empty($definitions['entity_traits'])) {
        $entity_manager->onFieldStorageDefinitionDelete($definitions['entity_traits']);
      }
    }
  }

  /**
   * Returns view or form displays for this bundle.
   *
   * @param string $entity_type_id
   *   The entity type ID of the display type to load.
   *
   * @return \Drupal\Core\Entity\Display\EntityDisplayInterface[]
   *   A list of matching displays.
   */
  protected function loadDisplays($entity_type_id) {
    $ids = \Drupal::entityQuery($entity_type_id)
      ->condition('id', $this->getPlugin()->traitEntityType() . '.' . $this->getOriginalId() . '.', 'STARTS_WITH')
      ->execute();
    if ($ids) {
      $storage = $this->entityManager()->getStorage($entity_type_id);
      return $storage->loadMultiple($ids);
    }
    return array();
  }
}