<?php

namespace Drupal\entity_trait\Entity;

interface EntityTraitUseInterface {

  /**
   * Get the entity trait config.
   *
   * @return \Drupal\entity_trait\Entity\EntityTraitConfigInterface;
   */
  public function getTraitConfig();

  /**
   * Get the entity trait plugin.
   *
   * @return \Drupal\entity_trait\EntityTraitPluginInterface
   */
  public function getTrait();

}