<?php

namespace Drupal\entity_trait\Entity;

interface EntityTraitConfigInterface {

  /**
   * Get the entity trait plugin.
   *
   * @return \Drupal\entity_trait\EntityTraitPluginInterface
   */
  public function getPlugin();

}