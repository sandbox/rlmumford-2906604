<?php

namespace Drupal\entity_trait\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the entity trait entity class.
 *
 * @ContentEntityType(
 *   id = "entity_trait_use",
 *   label = @Translation("Entity Trait"),
 *   label_singular = @Translation("trait"),
 *   label_plural = @Translation("traits"),
 *   label_count = @PluralTranslation(
 *     singular = "a trait",
 *     plural = "@count traits",
 *   ),
 *   handlers = {},
 *   base_table = "entity_trait",
 *   data_table = "entity_trait_data",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "label",
 *     "bundle" = "trait_config",
 *   }
 */
class EntityTraitUse extends ContentEntityBase implements EntityTraitUseInterface {

  /**
   * {@inheritdoc}
   */
  public function getTraitConfig() {
    return EntityTraitConfig::load($this->bundle());
  }

  /**
   * {@inheritdoc}
   */
  public function getTrait() {
    $config = $this->getTraitConfig();
    return $config->getPlugin();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['entity'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Entity'))
      ->setDescription(t('The entity this trait use relates to.'));
    $fields['trait'] = BaseFieldDefinition::create('list_text')
      ->setLabel(t('Trait'))
      ->setDescription(t('The type of trait this is.'))
      ->setSetting('allowed_values_function', 'entity_trait_plugin_options');
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    if ($config = $this->getTraitConfig()) {
      $fields = $config->getPlugin()->traitFieldDefinitions();

      $fields['entity'] = clone $base_field_definitions['entity'];
      $fields['entity']->setSetting('target_type', $config->getTargetEntityTypeId());
      return $fields;
    }

    return [];
  }
}