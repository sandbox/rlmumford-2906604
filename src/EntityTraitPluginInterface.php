<?php

namespace Drupal\entity_trait;

interface EntityTraitPluginInterface {

  /**
   * Get a list of required fields for this trait.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface[]
   *   An array of field definitions for the trait keyed by field name.
   */
  public function traitFieldDefinitions();

  /**
   * Get the entity type used for storing trait information.
   *
   * @return string
   */
  public function traitEntityType();

}