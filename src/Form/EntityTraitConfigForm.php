<?php

namespace Drupal\entity_trait\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for entity trait config forms.
 */
class EntityTraitConfigForm extends BundleEntityFormBase {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Constructs the EntityTraitConfigForm object.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   */
  public function __construct(EntityManagerInterface $entity_manager) {
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form['#title'] = $this->t('Configure %label entity trait', ['%label' => $this->entity->label()]);

    $form['label'] = [
      '#title' => $this->t('Label'),
      '#type' => 'textfield',
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
      '#size' => 30,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#disabled' => TRUE,
    ];

    $form['plugin'] = [
      '#type' => 'select',
      '#title' => $this->t('Plugin'),
      '#disabled' => TRUE,
      '#options' => entity_trait_plugin_options(),
      '#default_value' => $this->entity->get('entity_trait'),
    ];

    $entity_type_bundle_options = [];
    foreach ($this->entityManager->getAllBundleInfo() as $entity_type => $bundles) {
      $entity_type_label = $this->entityManager->getDefinition($entity_type)->label();
      foreach ($bundles as $bundle => $info) {
        $entity_type_bundle_options[$entity_type_label][$entity_type."::".$bundle] = $info['label'];
      }
    }
    $form['target_entity_type_bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity Type & Bundle'),
      '#default_value' => $this->entity->get('target_entity_type_id').'::'.$this->entity->get('target_bundle_id'),
      '#options' => $entity_type_bundle_options,
      '#disabled' => TRUE,
    ];

    if ($this->operation == 'add') {
      $form['#title'] = $this->t('Add a Entity Trait');
      $form['id']['#disabled'] = FALSE;
      $form['id']['#machine_name'] = [
        'exists' => ['Drupal\entity_trait\Entity\EntityTraitConfig', 'load'],
        'source' => 'label',
      ];
      $form['plugin']['#disabled'] = FALSE;
      $form['plugin']['#required'] = TRUE;
      $form['target_entity_type_bundle']['#disabled'] = FALSE;
      $form['target_entity_type_bundle']['#required'] = TRUE;

      return $form;
    }

    $entity_type_label = $this->entityManager->getDefinition($this->entity->get('target_entity_type_id'))->label();
    $form['field_map'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Trait Field'),
        $this->t('%type Field', ['%type' => $entity_type_label]),
      ],
      '#empty' => $this->t('This trait has no fields'),
    ];
    $field_map = $this->entity->get('field_map');
    foreach ($this->entity->getPlugin()->traitFieldDefinitions() as $field_name => $definition) {
      $entity_field_options = [
        '__no_value' => $this->t('No Value'),
        '__direct' => $this->t('Store directly on Trait'),
        '__create' => $this->t('Create a new field'),
      ];
      foreach ($this->entityManager->getFieldDefinitions($this->entity->get('target_entity_type_id'), $this->entity->get('target_bundle_id')) as $target_field_name => $target_definition) {
        // Only map similar fields.
        if ($target_definition->getType() != $definition->getType()) {
          continue;
        }

        // If the field is a list type, only map if the values intersect.
        if (in_array($target_definition->getType(), ['list_string', 'list_float', 'list_integer'])) {
          $allowed_values = options_allowed_values($definition);
          $target_allowed_values = options_allowed_values($target_definition);
          $intersect = array_intersect_key($target_allowed_values, $allowed_values);

          if (count($intersect) !== count($target_allowed_values)) {
            continue;
          }
        }

        // If the field is an entityreference check they reference the same type.
        if ($target_definition->getType() == 'entityreference') {
          if ($target_definition->getSetting('target_type') !== $definition->getSetting('target_type')) {
            continue;
          }

          // @todo: Check target bundles.
        }

        $entity_field_options[$target_field_name] = $target_definition->label();
      }

      $form['field_map'][$field_name]['trait_field'] = [
        '#markup' => $this->t('%label (%type)', ['%label' => $definition->getLabel(), '%type' => $definition->getType()]),
      ];
      $form['field_map'][$field_name]['entity_field'] = [
        '#type' => 'select',
        '#title' => $this->t('Mapped Field'),
        '#title_display' => 'invisible',
        '#options' => $entity_field_options,
        '#default_value' => $field_map[$field_name],
      ];
    }

    return $form;
  }
}