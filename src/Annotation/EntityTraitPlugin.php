<?php

namespace Drupal\entity_trait\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an Ranking Game annotation object.
 *
 * Plugin Namespace: Plugin\EntityTraitPlugin
 *
 * @Annotation
 */
class EntityTraitPlugin extends Plugin {

  /**
   * The plugin id.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the action plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;

}