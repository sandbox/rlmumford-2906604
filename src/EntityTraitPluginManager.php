<?php

namespace Drupal\entity_trait;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides a plugin manager for Entity Trait plugins.
 */
class EntityTraitPluginManager extends DefaultPluginManager {

  /**
   * Constructs a EntityTraitPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/EntityTraitPlugin',
      $namespaces,
      $module_handler,
      'Drupal\entity_trait\EntityTraitPluginInterface',
      'Drupal\entity_trait\Annotation\EntityTraitPlugin'
    );
    $this->alterInfo('entity_trait_plugin_info');
    $this->setCacheBackend($cache_backend, 'entity_trait_plugins');
  }
}